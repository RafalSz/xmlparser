from xml.dom import minidom
__author__ = 'ian'


def parseXml(filename):
    xmldoc = minidom.parse(filename)
    bookslist = xmldoc.getElementsByTagName('book')
    booksinfo = []
    bookitem = []
    for book in bookslist:
        bookitem.append(book.attributes['id'].value)
        author = book.getElementsByTagName('author')[0]
        title = book.getElementsByTagName('title')[0]
        genre = book.getElementsByTagName('genre')[0]
        price = book.getElementsByTagName('price')[0]
        publish = book.getElementsByTagName('publish_date')[0]
        description = book.getElementsByTagName('description')[0]
        bookitem.append(str(author.firstChild.data) + str(title.firstChild.data) + str(genre.firstChild.data) + str(price.firstChild.data) + str(publish.firstChild.data) + str(description.firstChild.data))
        booksinfo.append(bookitem)
        bookitem = []
    for book in booksinfo:
        print(book)
if __name__ == "__main__":
    parseXml('Books.xml')
